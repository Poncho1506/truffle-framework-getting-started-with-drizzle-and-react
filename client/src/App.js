import React, { Component } from 'react';
import './App.css';
import ReadString from "./ReadString";
import SetString from "./SetString";

class App extends Component {
  state = { loading: true, drizzleState: null };
  /**
   * We are going to be using two state variables here:
   *
   * loading — Indicates if Drizzle has finished initializing and the app is ready. The initialization process includes instantiating web3 and our smart contracts, fetching any available Ethereum accounts and listening (or, in cases where subscriptions are not supported: polling) for new blocks.
   * drizzleState — This is where we will store the state of the Drizzle store in our top-level component. If we can keep this state variable up-to-date, then we can simply use simple props and state to work with Drizzle (i.e. you don't have to use any Redux or advanced React patterns).
   */

  componentDidMount() {
    const { drizzle } = this.props;
	  /**
	   * First, we grab the drizzle instance from the props, then we call drizzle.store.subscribe and pass in a callback function. This callback function is called whenever the Drizzle store is updated. Note that this store is actually a Redux store under the hood, so this might look familiar if you've used Redux previously.
	   */

	  // subscribe to changes in the store
	  /**
	   * Note that we assign the return value of the subscribe() to a class variable this.unsubscribe. This is because it is always good practice to unsubscribe from any subscriptions you have when the component un-mounts. In order to do this, we save a reference to that subscription (i.e. this.unsubscribe)
	   * This will safely unsubscribe when the App component un-mounts so we can prevent any memory leaks.
	   */
    this.unsubscribe = drizzle.store.subscribe(() => {

      // every time the store updates, grab the state from drizzle
	    /**
	     * Whenever the store is updated, we will try to grab the state with drizzle.store.getState() and then if Drizzle is initialized and ready, we will set loading to false, and also update the drizzleState state variable.
	     * By doing this, drizzleState will always be up-to-date and we also know exactly when Drizzle is ready so we can use a loading component to let the user know.
	     */
      const drizzleState = drizzle.store.getState();

      // check to see if it's ready. if so, update local component state
      if (drizzleState.drizzleStatus.initialized) {
        this.setState({ loading: false, drizzleState });
      }
    });
  }

  // It is always good practice to unsubscribe from any subscriptions you have when the component un-mounts. In order to do this, we save a reference to that subscription
  compomentWillUnmount() {
    this.unsubscribe();
  }

	/**
	 * drizzle instance and drizzleState
	 * For the most part, drizzleState is there for you to read information from (i.e. contract state variables, return values, transaction status, account data, etc.), whereas the drizzle instance is what you will use to actually get stuff done (i.e. call contract methods, the Web3 instance, etc.).
	 */

	render() {
		if (this.state.loading) return "Loading Drizzle...";
		return (
			<div className="App">
				<ReadString
					drizzle={this.props.drizzle}
					drizzleState={this.state.drizzleState}
				/>
				<SetString
					drizzle={this.props.drizzle}
					drizzleState={this.state.drizzleState}
				/>
			</div>
		);
	}
}

export default App;
